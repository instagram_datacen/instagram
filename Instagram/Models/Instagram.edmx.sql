
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 11/06/2017 22:04:09
-- Generated from EDMX file: D:\Data Cen\Instagram\Instagram\Models\Instagram.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Instagram];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'UserSet'
CREATE TABLE [dbo].[UserSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Fname] nvarchar(max)  NOT NULL,
    [Lname] nvarchar(max)  NOT NULL,
    [Username] nvarchar(max)  NOT NULL,
    [Password] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'PictureSet'
CREATE TABLE [dbo].[PictureSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Pname] nvarchar(max)  NOT NULL,
    [like] nvarchar(max)  NOT NULL,
    [User_id] nvarchar(max)  NOT NULL,
    [UserId] int  NOT NULL
);
GO

-- Creating table 'CommentSet'
CREATE TABLE [dbo].[CommentSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PID] nvarchar(max)  NOT NULL,
    [UID] nvarchar(max)  NOT NULL,
    [Comment_Text] nvarchar(max)  NOT NULL,
    [UserId] int  NOT NULL,
    [PictureId] int  NOT NULL
);
GO

-- Creating table 'FollowSet'
CREATE TABLE [dbo].[FollowSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Following_ID] nvarchar(max)  NOT NULL,
    [Followed_ID] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'UserFollow'
CREATE TABLE [dbo].[UserFollow] (
    [User_Id] int  NOT NULL,
    [Follow_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'UserSet'
ALTER TABLE [dbo].[UserSet]
ADD CONSTRAINT [PK_UserSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PictureSet'
ALTER TABLE [dbo].[PictureSet]
ADD CONSTRAINT [PK_PictureSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CommentSet'
ALTER TABLE [dbo].[CommentSet]
ADD CONSTRAINT [PK_CommentSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'FollowSet'
ALTER TABLE [dbo].[FollowSet]
ADD CONSTRAINT [PK_FollowSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [User_Id], [Follow_Id] in table 'UserFollow'
ALTER TABLE [dbo].[UserFollow]
ADD CONSTRAINT [PK_UserFollow]
    PRIMARY KEY CLUSTERED ([User_Id], [Follow_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [User_Id] in table 'UserFollow'
ALTER TABLE [dbo].[UserFollow]
ADD CONSTRAINT [FK_UserFollow_User]
    FOREIGN KEY ([User_Id])
    REFERENCES [dbo].[UserSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Follow_Id] in table 'UserFollow'
ALTER TABLE [dbo].[UserFollow]
ADD CONSTRAINT [FK_UserFollow_Follow]
    FOREIGN KEY ([Follow_Id])
    REFERENCES [dbo].[FollowSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserFollow_Follow'
CREATE INDEX [IX_FK_UserFollow_Follow]
ON [dbo].[UserFollow]
    ([Follow_Id]);
GO

-- Creating foreign key on [UserId] in table 'PictureSet'
ALTER TABLE [dbo].[PictureSet]
ADD CONSTRAINT [FK_UserPicture]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[UserSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserPicture'
CREATE INDEX [IX_FK_UserPicture]
ON [dbo].[PictureSet]
    ([UserId]);
GO

-- Creating foreign key on [UserId] in table 'CommentSet'
ALTER TABLE [dbo].[CommentSet]
ADD CONSTRAINT [FK_UserComment]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[UserSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserComment'
CREATE INDEX [IX_FK_UserComment]
ON [dbo].[CommentSet]
    ([UserId]);
GO

-- Creating foreign key on [PictureId] in table 'CommentSet'
ALTER TABLE [dbo].[CommentSet]
ADD CONSTRAINT [FK_PictureComment]
    FOREIGN KEY ([PictureId])
    REFERENCES [dbo].[PictureSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PictureComment'
CREATE INDEX [IX_FK_PictureComment]
ON [dbo].[CommentSet]
    ([PictureId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------