
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 12/14/2017 14:04:41
-- Generated from EDMX file: D:\Data Cen\Project\instagram\Instagram\Instagram.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Instagram];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_UserPicture]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PictureSet] DROP CONSTRAINT [FK_UserPicture];
GO
IF OBJECT_ID(N'[dbo].[FK_UserFollow_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserFollow] DROP CONSTRAINT [FK_UserFollow_User];
GO
IF OBJECT_ID(N'[dbo].[FK_UserFollow_Follow]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserFollow] DROP CONSTRAINT [FK_UserFollow_Follow];
GO
IF OBJECT_ID(N'[dbo].[FK_UserComment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CommentSet] DROP CONSTRAINT [FK_UserComment];
GO
IF OBJECT_ID(N'[dbo].[FK_PictureComment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CommentSet] DROP CONSTRAINT [FK_PictureComment];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[UserSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserSet];
GO
IF OBJECT_ID(N'[dbo].[FollowSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[FollowSet];
GO
IF OBJECT_ID(N'[dbo].[PictureSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PictureSet];
GO
IF OBJECT_ID(N'[dbo].[CommentSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CommentSet];
GO
IF OBJECT_ID(N'[dbo].[UserFollow]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserFollow];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'UserSet'
CREATE TABLE [dbo].[UserSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Username] nvarchar(max)  NOT NULL,
    [Password] nvarchar(max)  NOT NULL,
    [Fristname] nvarchar(max)  NOT NULL,
    [Lastname] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'FollowSet'
CREATE TABLE [dbo].[FollowSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Following_ID] nvarchar(max)  NOT NULL,
    [Followed_ID] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'PictureSet'
CREATE TABLE [dbo].[PictureSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Path] nvarchar(max)  NOT NULL,
    [Caption] nvarchar(max)  NOT NULL,
    [Like] nvarchar(max)  NOT NULL,
    [UserId] int  NOT NULL
);
GO

-- Creating table 'CommentSet'
CREATE TABLE [dbo].[CommentSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Comment_Text] nvarchar(max)  NOT NULL,
    [Date_Time] nvarchar(max)  NOT NULL,
    [UserId] int  NOT NULL,
    [PictureId] int  NOT NULL
);
GO

-- Creating table 'UserFollow'
CREATE TABLE [dbo].[UserFollow] (
    [User_Id] int  NOT NULL,
    [Follow_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'UserSet'
ALTER TABLE [dbo].[UserSet]
ADD CONSTRAINT [PK_UserSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'FollowSet'
ALTER TABLE [dbo].[FollowSet]
ADD CONSTRAINT [PK_FollowSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PictureSet'
ALTER TABLE [dbo].[PictureSet]
ADD CONSTRAINT [PK_PictureSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CommentSet'
ALTER TABLE [dbo].[CommentSet]
ADD CONSTRAINT [PK_CommentSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [User_Id], [Follow_Id] in table 'UserFollow'
ALTER TABLE [dbo].[UserFollow]
ADD CONSTRAINT [PK_UserFollow]
    PRIMARY KEY CLUSTERED ([User_Id], [Follow_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [UserId] in table 'PictureSet'
ALTER TABLE [dbo].[PictureSet]
ADD CONSTRAINT [FK_UserPicture]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[UserSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserPicture'
CREATE INDEX [IX_FK_UserPicture]
ON [dbo].[PictureSet]
    ([UserId]);
GO

-- Creating foreign key on [User_Id] in table 'UserFollow'
ALTER TABLE [dbo].[UserFollow]
ADD CONSTRAINT [FK_UserFollow_User]
    FOREIGN KEY ([User_Id])
    REFERENCES [dbo].[UserSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Follow_Id] in table 'UserFollow'
ALTER TABLE [dbo].[UserFollow]
ADD CONSTRAINT [FK_UserFollow_Follow]
    FOREIGN KEY ([Follow_Id])
    REFERENCES [dbo].[FollowSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserFollow_Follow'
CREATE INDEX [IX_FK_UserFollow_Follow]
ON [dbo].[UserFollow]
    ([Follow_Id]);
GO

-- Creating foreign key on [UserId] in table 'CommentSet'
ALTER TABLE [dbo].[CommentSet]
ADD CONSTRAINT [FK_UserComment]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[UserSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserComment'
CREATE INDEX [IX_FK_UserComment]
ON [dbo].[CommentSet]
    ([UserId]);
GO

-- Creating foreign key on [PictureId] in table 'CommentSet'
ALTER TABLE [dbo].[CommentSet]
ADD CONSTRAINT [FK_PictureComment]
    FOREIGN KEY ([PictureId])
    REFERENCES [dbo].[PictureSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PictureComment'
CREATE INDEX [IX_FK_PictureComment]
ON [dbo].[CommentSet]
    ([PictureId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------